import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import { Product } from './product.class';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

    private static productslist: Product[] = null;
    private products$: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);
    constructor(private http: HttpClient) { }

    getProducts(): Observable<Product[]> {

      return this.http.get<Product[]>('http://localhost:3000/products/getProducts');
    }

    create(prod: Product): Observable<Product[]> {

        return this.http.post<Product[]>('http://localhost:3000/products/add', prod);
    }

    update(prod: Product): Observable<Product[]>{
      return this.http.patch<Product[]>('http://localhost:3000/product1/update', prod);
    }


    delete(id: number): Observable<number>{
        return this.http.delete<number>('http://localhost:3000/product1/delete/' + id);

    }
}
