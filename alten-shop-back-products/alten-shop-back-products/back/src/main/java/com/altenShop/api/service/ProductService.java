package com.altenShop.api.service;

import com.altenShop.api.model.Product;
import com.altenShop.api.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Optional<Product> getProduct(final Long id){
        return productRepository.findById(id);
    }

    public Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public void deleteProduct(final Long id){
        productRepository.deleteById(id);
    }

    public Product saveProduct(Product product){
        Product savedProduct = productRepository.save(product);
        return savedProduct;
    }

    public Product updateProduct(Product product){
        Product updatedProduct = productRepository.save(product);
        return updatedProduct;
    }
}
