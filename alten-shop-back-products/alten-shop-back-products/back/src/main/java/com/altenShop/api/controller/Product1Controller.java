package com.altenShop.api.controller;

import com.altenShop.api.model.Product;
import com.altenShop.api.service.ProductService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/product1")
@CrossOrigin(origins = "http://localhost:4200/")
public class Product1Controller {

    private ProductService productService;

    public Product1Controller(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/details")
    public Optional<Product> getProductDetails(@RequestParam long id){
        return productService.getProduct(id);
    }

    @PatchMapping("/update")
    public void updateProduct(@RequestBody Product product){
        productService.updateProduct(product);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteProduct(@PathVariable("id") long id){
        productService.deleteProduct(id);
    }
}
